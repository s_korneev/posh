function backup-discovery() {
    $data = @()
    if(Get-Service | where name -EQ 'MMS'){
        $data += @{'{#BACKUP}' = 'Acronis'}
    }
    return (New-Object PSObject | Add-Member -PassThru NoteProperty data $data) | ConvertTo-Json
}
function get-Acronislastbackupevent{
    $StartTime = (Get-Date).AddDays(-2)
    Get-WinEvent -FilterHashtable @{Logname="Application"; ProviderName="Acronis Backup Agent Core"; StartTime=$StartTime} -MaxEvents 1 -ErrorAction SilentlyContinue
}

function get-Acronislastbackupexitmessage(){
    $event = get-Acronislastbackupevent
    if($event){
        return $event.TimeCreated.tostring() + " " + ($event.message -split '[\r\n]')[1]
    }
    else{
        return "Last backup was more than two days"
    }
}

function get-Acronislastbackupexitcode(){
    $event = get-Acronislastbackupevent
    if($event){
        return [regex]::match(($event.message -split '[\r\n]')[0],'\d+').Value
    }
    else{
        return 255
    }
}

switch ($args[0]){
    "backup-discovery" {backup-discovery}
    "get-Acronislastbackupexitcode" {get-Acronislastbackupexitcode}
    "get-Acronislastbackupexitmessage" {get-Acronislastbackupexitmessage}
}
